#pragma once

#include <array>
#include <atomic>

#include <cstdint>

struct Balancer {
    std::atomic_uint8_t toggle{0};

    constexpr std::uint64_t traverse() noexcept
    {
        return toggle.fetch_xor(1, std::memory_order_acq_rel);
    }
};

template <std::uint64_t size>
struct Merger {
    std::array<Merger<size / 2>, 2> half;
    std::array<Balancer, size / 2>  balancers;

    constexpr std::uint64_t traverse(const std::uint64_t input)
    {
        const auto output = half[input % 2].traverse(input / 2);
        return 2 * output + balancers[output].traverse();
    }
};

template <>
struct Merger<2> {
    Balancer balancer;

    constexpr static std::uint64_t size = 2;

    constexpr std::uint64_t traverse(std::uint64_t)
    {
        return balancer.traverse();
    }
};

template <std::uint64_t size_>
struct Bitonic {
    std::array<Bitonic<size_ / 2>, 2> half;
    Merger<size_>                     merger;

    constexpr static auto size = size_;

    constexpr std::uint64_t traverse(const std::uint64_t input)
    {
        return merger.traverse(half[input % 2].traverse(input / 2));
    }
};

template <>
struct Bitonic<2> : Merger<2> {};