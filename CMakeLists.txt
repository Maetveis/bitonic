cmake_minimum_required(VERSION 3.15)
cmake_policy(VERSION 3.15)

project(Bitonic CXX)

find_package(benchmark REQUIRED)

set(CMAKE_CXX_EXTENSIONS off)

add_library(project_options INTERFACE)
target_compile_features(project_options INTERFACE cxx_std_17)

add_library(project_warnings INTERFACE)
target_compile_options(project_warnings INTERFACE
    -Wall
    -Wextra
    -Werror
    -Wunused
    -Wshadow
    -pedantic
    -pedantic-errors)

add_executable(benchmark
    Bitonic.hpp
    benchmark.cpp)

target_link_libraries(benchmark 
    project_options
    project_warnings
    benchmark::benchmark_main)

add_executable(random_test
    Bitonic.hpp
    random_test.cpp)

target_link_libraries(random_test 
    project_options
    project_warnings)