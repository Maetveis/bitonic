# C++ Compile time generated bitonic balancer networks

A simple recursive template implementation of lock-free bitonic
balancer networks.

## Testing
A randomized test to verify the distribution of the output can be seen in [random_test.cpp](random_test.cpp)

Additional tests to verify the behaviour with multiple threads could be added.

## Benchmarks
Some benchmarks were performed to evaluate the scaling of the networks
with different number of threads.

The benchmarks were performed on system with a Ryzen 2 2700X octa core processor.

The figure shows cpu time overhead relative to single threaded execution for a single traversal,
due to contention. \
See [benchmark.cpp](benchmark.cpp) for the benchmark performed.

![benchmark results](stats/percentage_overhead.png)

Further benchmarks could be added to showcase compile times, or binary size according to the size of
the network.

## Building
This project uses cmake.

For the benchmark [Google benchmark](https://github.com/google/benchmark) is used.