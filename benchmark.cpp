#include "Bitonic.hpp"

#include <benchmark/benchmark.h>

template <class BalancerT>
void BM_Bitonic(benchmark::State& state)
{
    BalancerT  bitonic;
    const auto index = state.thread_index & (BalancerT::size - 1);
    for (auto _ : state) {
        benchmark::DoNotOptimize(bitonic.traverse(index));
    }
}

BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<2>)->Threads(1);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<2>)->Threads(2);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<2>)->Threads(3);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<2>)->Threads(4);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<2>)->Threads(5);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<2>)->Threads(6);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<2>)->Threads(7);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<2>)->Threads(8);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<2>)->Threads(9);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<2>)->Threads(10);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<2>)->Threads(11);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<2>)->Threads(12);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<2>)->Threads(13);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<2>)->Threads(14);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<2>)->Threads(15);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<2>)->Threads(16);

BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<4>)->Threads(1);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<4>)->Threads(2);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<4>)->Threads(3);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<4>)->Threads(4);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<4>)->Threads(5);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<4>)->Threads(6);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<4>)->Threads(7);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<4>)->Threads(8);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<4>)->Threads(9);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<4>)->Threads(10);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<4>)->Threads(11);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<4>)->Threads(12);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<4>)->Threads(13);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<4>)->Threads(14);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<4>)->Threads(15);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<4>)->Threads(16);

BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<8>)->Threads(1);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<8>)->Threads(2);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<8>)->Threads(3);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<8>)->Threads(4);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<8>)->Threads(5);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<8>)->Threads(6);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<8>)->Threads(7);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<8>)->Threads(8);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<8>)->Threads(9);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<8>)->Threads(10);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<8>)->Threads(11);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<8>)->Threads(12);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<8>)->Threads(13);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<8>)->Threads(14);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<8>)->Threads(15);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<8>)->Threads(16);

BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<16>)->Threads(1);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<16>)->Threads(2);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<16>)->Threads(3);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<16>)->Threads(4);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<16>)->Threads(5);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<16>)->Threads(6);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<16>)->Threads(7);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<16>)->Threads(8);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<16>)->Threads(9);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<16>)->Threads(10);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<16>)->Threads(11);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<16>)->Threads(12);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<16>)->Threads(13);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<16>)->Threads(14);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<16>)->Threads(15);
BENCHMARK_TEMPLATE(BM_Bitonic, Bitonic<16>)->Threads(16);