#include "Bitonic.hpp"

#include <algorithm>
#include <iostream>
#include <random>
#include <string>

constexpr std::uint64_t N = 128;

Bitonic<N> bitonic;

int main()
{
    std::binomial_distribution<std::uint64_t> d(N - 1, 0.5);
    std::random_device                        g;

    std::array<std::uint64_t, N> input_dist{};
    std::array<std::uint64_t, N> output_dist{};

    for (unsigned i = 0; i < 100000; ++i) {
        const auto input = d(g);
        ++input_dist[input];
        ++output_dist[bitonic.traverse(input)];
    }

    std::cout << "Input distribution:\n";
    for (unsigned i = 0; i < 128; ++i) {
        std::cout << "input_dist[" << i << "] = " << input_dist[i] << '\n';
    }

    std::cout << "\nOutput distribution:\n";
    for (unsigned i = 0; i < 128; ++i) {
        std::cout << "output_dist[" << i << "] = " << output_dist[i] << '\n';
    }

    std::cerr << "---------\n";

    const auto input_time = *std::max_element(input_dist.begin(), input_dist.end());
    std::cerr << "Input would take " << input_time << " units of time\n";

    const auto output_time = *std::max_element(output_dist.begin(), output_dist.end());
    std::cerr << "Output would take " << output_time << " units of time\n";

    std::cerr << "Speedup: " << 100.0 * static_cast<double>(input_time) / output_time << "%\n";
}